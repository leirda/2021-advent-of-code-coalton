; SPDX-FileCopyrightText: 2021 leirda <leirda@aurible.fr>
;
; SPDX-License-Identifier: WTFPL

(asdf:defsystem #:aoc2021
  :description "My partition to the 2021 Advent Of Code in Coalton."
  :author "leirda"
  :license "WTFPL"
  :depends-on (#:coalton)
  :pathname "src/"
  :serial t
  :components ((:file "package")
               (:file "coalton-utils")
               (:file "day4")
               (:static-file "input/bingo")
               (:file "day3")
               (:static-file "input/power-consumption")
               (:file "day2")
               (:static-file "input/commands")
               (:file "day1")
               (:static-file "input/measurements")))
