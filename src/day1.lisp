; SPDX-FileCopyrightText: 2021 leirda <leirda@aurible.fr>
;
; SPDX-License-Identifier: WTFPL

(in-package #:cl-user)

(defpackage #:day1
  (:use #:coalton #:coalton-library #:coalton-utils)
  (:export #:answers))

(in-package #:day1)

(coalton-toplevel

  (declare answers (Tuple Integer Integer))
  (define answers
    (Tuple (pipe measurements depth-diffs count-greater)
           (pipe measurements sum-by-three depth-diffs count-greater)))


  (declare count-greater ((List Ord) -> Integer))
  (define (count-greater diffs)
    (fold
      (fn (elem total)
        (match elem
          ((GT) (+ 1 total))
          (_ total)))
      0
      diffs))

  (declare sum-by-three ((List Integer) -> (List Integer)))
  (define (sum-by-three elems)
    (let ((sum3 (fn (x y z) (+ (+ x y) z)))
          (elems+1 (optionalDefault Nil (tail elems)))
          (elems+2 (optionalDefault Nil (tail elems+1))))
      (zipWith3 sum3 elems elems+1 elems+2)))

  (declare depth-diffs (Ord :a => ((List :a) -> (List Ord))))
  (define (depth-diffs elems)
    (zipWith <=> (optionalDefault Nil (tail elems)) elems))

  (declare measurements (List Integer))
  (define measurements
    (let ((file (aoc-relative "input/measurements"))
          (content
            (fromSome (concat-string "Error reading the file " file)
                      (read-file-string file))))
      (pipe content
            unpack-string
            (splitBy (== #\Newline))
            (map
              (compose
                (compose
                  (fromSome "not an integer!")
                  parse-int)
                pack-string))))))
