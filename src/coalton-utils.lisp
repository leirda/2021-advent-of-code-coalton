; SPDX-FileCopyrightText: 2021 leirda <leirda@aurible.fr>
;
; SPDX-License-Identifier: WTFPL

(in-package #:cl-user)

(defpackage #:coalton-utils
  (:use #:coalton #:coalton-library)
  (:export #:aoc-relative
           #:read-file-string
           #:mapFst #:mapSnd
           #:takeWhile #:dropWhile
           #:span #:break
           #:splitBy
           #:split
           #:lines #:words
           #:elem #:last #:init
           #:optionalDefault
           #:resultFromOptional
           #:fromErr))

(in-package #:coalton-utils)

(coalton-toplevel

  ;;
  ;; IO
  ;;

  ;; Thank you Robert! :D
  ;; => https://github.com/stylewarning/aoc/blob/25573eaa2c0243ac4af960ad1ceacc5e5a4d83cb/2021/utils.lisp#L8
  ;; I was too lazy to think about files in coalton
  (declare read-file-string (String -> (Optional String)))
  (define (read-file-string filename)
    "Read the file FILENAME from disk into a string."
    (lisp (Optional String) (filename)
          (cl:let ((content (cl:ignore-errors
                              (uiop:read-file-string filename))))
                  (cl:if (cl:null content) None (Some content)))))

  (declare aoc-relative (String -> String))
  (define (aoc-relative rel-path)
    (lisp String (rel-path)
          (cl:namestring (asdf:system-relative-pathname ':aoc2021 rel-path))))


  ;;
  ;; Tuples
  ;;

  (declare mapFst ((:a -> :b) -> (Tuple :a :c) -> (Tuple :b :c)))
  (define (mapFst f x)
    (match x
      ((Tuple a b) (Tuple (f a) b))))

  (declare mapSnd ((:a -> :b) -> (Tuple :c :a) -> (Tuple :c :b)))
  (define (mapSnd f x)
    (match x
      ((Tuple a b) (Tuple a (f b)))))


  ;;
  ;; List
  ;;

  (declare takeWhile ((:a -> Boolean) -> (List :a) -> (List :a)))
  (define (takeWhile p xs)
    "Take elements from XS as long as P is true."
    (match xs
      ((Nil) Nil)
      ((Cons x xs)
       (if (p x)
           (Cons x (takeWhile p xs))
           Nil))))

  (declare dropWhile ((:a -> Boolean) -> (List :a) -> (List :a)))
  (define (dropWhile p xs)
    "Drop elements from XS as long as P is true."
    (match xs
      ((Nil) Nil)
      ((Cons x xs)
       (if (p x)
           (dropWhile p xs)
           (Cons x xs)))))

  (declare span ((:a -> Boolean) -> (List :a) -> (Tuple (List :a) (List :a))))
  (define (span p xs)
    "Split XS at the first element for which P is no longer true."
    (match xs
      ((Nil) (Tuple Nil Nil))
      ((Cons x xs)
       (if (p x)
           (let ((rs (span p xs))
                 (ys (fst rs)) (zs (snd rs)))
             (Tuple (Cons x ys) zs))
           (Tuple Nil (Cons x xs))))))

  (declare break ((:a -> Boolean) -> (List :a) -> (Tuple (List :a) (List :a))))
  (define (break p xs)
    "Split XS at the first element for which P become true."
    (match xs
      ((Nil) (Tuple Nil Nil))
      ((Cons x xs)
       (if (p x)
           (Tuple Nil (Cons x xs))
           (let ((rs (break p xs))
                 (ys (fst rs)) (zs (snd rs)))
             (Tuple (Cons x ys) zs))))))

  (declare splitBy ((Eq :a) => ((:a -> Boolean) -> (List :a) -> (List (List :a)))))
  (define (splitBy p xs)
    "Split XS at each elements satisfying P."
    (match xs
      ((Nil) Nil)
      (xs
        (let ((rs (break p xs))
              (ys (fst rs)) (zs (snd rs)))
          (if (== Nil ys)
              (splitBy p (dropWhile p zs))
              (Cons ys (splitBy p (dropWhile p zs))))))))

  (declare split ((Eq :a) => (:a -> (List :a) -> (List (List :a)))))
  (define (split x xs)
    "Split XS at each elements equals to X."
    (splitBy (== x) xs))

  (declare lines ((List Char) -> (List (List Char))))
  (define (lines str)
    (split #\Newline str))

  (declare words ((List Char) -> (List (List Char))))
  (define (words str)
    (splitBy (fn (ch)
                 (or (== #\Newline ch)
                     (== #\Space ch)))
             str))

  (declare elem ((Eq :a) => (:a -> (List :a) -> Boolean)))
  (define (elem a xs)
    (any (== a) xs))

  (declare last ((List :a) -> (Optional :a)))
  (define (last xs)
    (head (drop (- (length xs) 1) xs)))

  (declare init ((List :a) -> (List :a)))
  (define (init xs)
    (match xs
      ((Nil) Nil)
      ((Cons x nil) xs)
      ((Cons x xs) (Cons x (init xs)))))


  ;;
  ;; Optional
  ;;

  (declare optionalDefault (:a -> (Optional :a) -> :a))
  (define (optionalDefault val opt)
    "Get the value of OPT defaulting to OPT if it is None."
    (match opt
      ((Some value) value)
      ((None) val)))


  ;;
  ;; Result
  ;;

  (declare resultFromOptional ((Result :e :a) -> (Optional :a) -> (Result :e :a)))
  (define (resultFromOptional res opt)
    "Convert OPT to a result with RES if it is None."
    (pipe opt
          (map Ok) 
          (optionalDefault res)))

  (declare fromErr ((Result String :a) -> :a))
  (define (fromErr res)
    "Get the value of RES, erroring with the provided error if it is Err."
    (match res
      ((Ok ok) ok)
      ((Err err) (error err)))))
