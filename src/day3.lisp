; SPDX-FileCopyrightText: 2021 leirda <leirda@aurible.fr>
;
; SPDX-License-Identifier: WTFPL

(in-package #:cl-user)

(defpackage #:day3
  (:use #:coalton #:coalton-library #:coalton-utils)
  (:export #:answers))

(in-package #:day3)

(coalton-toplevel

  (define answers
    (let ((diagnostic (transpose report))
          (frequences (map (compose most-common bits-count) diagnostic))
          (rates (γ/ε (map fromErr frequences))))
      (match rates
        ((Tuple γ ε) (* γ ε)))))

  (define-type Bit Zero One)

  (declare char->bit (Char -> (Result String Bit)))
  (define (char->bit ch)
    (match ch
      (#\0 (Ok Zero))
      (#\1 (Ok One))
      (inv (Err (concat-string "Invalid char: "
                               (pack-string (Cons inv Nil)))))))

  (declare bits->int ((List Bit) -> Integer))
  (define (bits->int bits)
    (let ((bit->int (fn (b) (match b ((Zero) 0) ((One) 1)))))
      (fold (fn (bit sum) (+ (* 2 sum) (bit->int bit))) 0 bits)))

  (declare bitxor (Bit -> Bit))
  (define (bitxor bit)
    (match bit ((Zero) One) ((One) Zero)))

  (declare bits-count ((List Bit) -> (Tuple Integer Integer)))
  (define (bits-count bits)
    (fold (fn (ch total)
            (match ch
              ((Zero) (mapFst (+ 1) total))
              ((One)  (mapSnd (+ 1) total))))
          (Tuple 0 0)
          bits))

  (declare γ/ε ((List Bit) -> (Tuple Integer Integer)))
  (define (γ/ε γ)
    (let ((ε (map bitxor γ))) (Tuple (bits->int γ) (bits->int ε))))

  (declare most-common ((Tuple Integer Integer) -> (Result String Bit)))
  (define (most-common counts)
    (cond
      ((> (fst counts) (snd counts)) (Ok Zero))
      ((< (fst counts) (snd counts)) (Ok One))
      ((== (fst counts) (snd counts)) (Err "both as common"))))


  (declare report (List (List Bit)))
  (define report
    (let ((file (aoc-relative "input/power-consumption"))
          (content
            (fromSome (concat-string "Error reading the input file " file)
                      (read-file-string file))))
      (pipe content
            unpack-string
            (splitBy (== #\Newline))
            (map (map char->bit))
            (map (map fromErr))))))
