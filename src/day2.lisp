; SPDX-FileCopyrightText: 2021 leirda <leirda@aurible.fr>
;
; SPDX-License-Identifier: WTFPL

(in-package #:cl-user)

(defpackage #:day2
  (:use #:coalton #:coalton-library #:coalton-utils)
  (:export #:answers))

(in-package #:day2)

(coalton-toplevel

  (define answers
    (Tuple 
      (match (fold add-position (Tuple 0 0) (map command->position commands))
        ((Tuple hpos depth) (* hpos depth)))
      (match (fold aim-position (Tuple3 0 0 0) commands)
        ((Tuple3 hpos depth _) (* hpos depth)))))

  (define-type Command
    (Forward Integer)
    (Up Integer)
    (Down Integer))


  (declare parse-command ((List Char) -> (Result String Command)))
  (define (parse-command s)
    (match (break (== #\Space) s)
      ((Tuple command? (Cons _ unit?))
       (let ((unit (pipe
                     (parse-int (pack-string unit?))
                     (resultFromOptional (Err "Invalid unit")))))
         (match (pack-string command?)
           ("forward" (map Forward unit))
           ("up"      (map Up unit))
           ("down"    (map Down unit))
           (unknown   (Err (concat-string "Unknown command: " unknown))))))
      (_
        (Err "Missing command or unit"))))

  (declare command->position (Command -> (Tuple Integer Integer)))
  (define (command->position command)
    (match command
      ((Forward unit) (Tuple unit 0))
      ((Up unit)      (Tuple 0    (negate unit)))
      ((Down unit)    (Tuple 0    unit))))

  (declare add-position
    ((Tuple Integer Integer) -> (Tuple Integer Integer)
     -> (Tuple Integer Integer)))
  (define (add-position a b)
    (Tuple (+ (fst a) (fst b))
           (+ (snd a) (snd b))))

  (declare aim-position (Command -> (Tuple3 Integer Integer Integer)
                                 -> (Tuple3 Integer Integer Integer)))
  (define (aim-position command from)
    (match (Tuple command from)
      ((Tuple (Forward unit) (Tuple3 hpos depth aim))
       (Tuple3 (+ hpos unit) (+ depth (* aim unit)) aim))
      ((Tuple (Up unit) (Tuple3 hpos depth aim))
       (Tuple3 hpos depth (- aim unit)))
      ((Tuple (Down unit) (Tuple3 hpos depth aim))
       (Tuple3 hpos depth (+ unit aim)))))


  (declare commands (List Command))
  (define commands
    (let ((file (aoc-relative "input/commands"))
          (content
            (fromSome (concat-string "Error reading the input file " file)
                      (read-file-string file))))
      (pipe content
            unpack-string
            (splitBy (== #\Newline))
            (map parse-command)
            (map fromErr)))))
