; SPDX-FileCopyrightText: 2021 leirda <leirda@aurible.fr>
;
; SPDX-License-Identifier: WTFPL

(in-package #:cl-user)

(defpackage #:day4
  (:use #:coalton #:coalton-library #:coalton-utils)
  (:export #:answers))

(in-package #:day4)

(coalton-toplevel

    (define answers
      (let ((winning-board
              (match bingo
                ((Tuple suite boards) (draw-until-wins suite boards))))
            (final-score
              (match winning-board
                ((Tuple drawns board)
                 (* (optionalDefault 0 (last drawns)) (score drawns board))))))
        final-score))

  (declare draw-until-wins ((List Integer) -> (List (List (List Integer)))
                                           -> (Tuple (List Integer) (List (List Integer)))))
  (define (draw-until-wins suite boards)
    (let ((rec (fn (n)
                 (match (find-winning-board (take n suite) boards)
                   ((None)
                    (if (== (length suite) n)
                        (error "There is no winning board")
                        (rec (+ 1 n))))
                   ((Some board) (Tuple (take n suite) board))))))
      (rec 5)))

  (declare find-winning-board ((List Integer) -> (List (List (List Integer)))
                                              -> (Optional (List (List Integer)))))
  (define (find-winning-board drawns boards)
    (find (board-wins? drawns) boards))

  (declare board-wins? ((List Integer) -> (List (List Integer)) -> Boolean))
  (define (board-wins? drawns board)
    (or
      (any (all ((flip elem) drawns)) board)
      (any (all ((flip elem) drawns)) (transpose board))))

  (declare score ((List Integer) -> (List (List Integer)) -> Integer))
  (define (score marked board)
    (sum (fold (fn (x unmarked)
                 (if (elem x marked)
                     unmarked
                     (Cons x unmarked)))
               Nil (concat board))))

  ; Instead of doing this hack, we should properly match on any substring
  (declare double-lines ((List Char) -> (List (List Char))))
  (define (double-lines str)
    (let ((piped (fn (str)
                   (match str
                     ((Nil) Nil)
                     ((Cons x Nil) (Cons x Nil))
                     ((Cons x (Cons y zs))
                      (if (and (== #\Newline x) (== #\Newline y))
                          (piped (Cons #\| zs))
                          (Cons x (piped (Cons y zs)))))))))
      (split #\| (piped str))))

  (declare bingo (Tuple (List Integer) (List (List (List Integer)))))
  (define bingo
    (let ((file (aoc-relative "input/bingo"))
          (content
            (fromSome "Error reading input file"
                      (read-file-string file))))
      (Tuple
        (pipe 
          content
          unpack-string
          (span (== #\Newline))
          fst
          (split #\,)
          (map
            (compose
              (compose
                (fromSome "Malformed input")
                parse-int)
              pack-string)))
        (pipe
          content
          unpack-string
          (span (== #\Newline))
          snd
          double-lines
          (map
            (compose
              (map
                (compose
                  (map
                    (compose
                      (compose
                        (fromSome "Malformed input")
                        parse-int)
                      pack-string))
                  words))
              lines)))))))
