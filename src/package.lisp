
;
; SPDX-License-Identifier: WTFPL

(defpackage #:aoc2021
  (:documentation "My participation of the 2021 Advent Of Code in Coalton.")
  (:use #:coalton
        #:coalton-library))
