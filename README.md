<!--
SPDX-FileCopyrightText: 2021 leirda <leirda@aurible.fr>

SPDX-License-Identifier: WTFPL
-->

# Advent of Code 2021 in Coalton

My take at the 2021 Advent of Code in Coalton.

Coalton is a statically typed language that integrates some types theories
concepts from Haskell in Common Lisp.

=> https://coalton-lang.github.io/

It's quite new and there's some hype around it in the Common Lisp community, and
I wanted to give it a try, as I enjoy both ML-styles languages and lisps.


***Note:***

I gave up doing the Advent Of Code this year in Coalton as I didn't had enough
time to complete it, work and rest in the same day.
I hoped to catch up during the weekend, but I was under too much pressure.

Here's some thoughts I had while using Coalton:

- The lack of tons of functions in the standard lib makes the languages very
  hard to use. This was expected, as one of the main goal of this AOC for
  Coalton was to complete the standard library anyway, but this was too much to
  handle for me unfortunately. I gave up when I realized that the ideal would be
  to make a parsing library for reading the inputs of the AOC. Using CL for this
  task doesn't interest me at the moment.
- It was pretty fun to implement some standard list manipulation functions, as
  the language is very pleasant to use and I love thinking about type and type
  classes. I think it's great to fix the inferred type system which has some
  bugs, but it's better to have to write explicitly function types than having
  to remove them to make the compiler happy.
- I think it's great to have both Common Lisp and Coalton available in dev, and
  it's neat that Coatlon can directly benefits of features from Common Lisp
  (asdf for project managements, different compilers, highly interactive nature,
  in the future perhaps the error system, etc).
- I'm concerned about the future of package management for Coalton though. We
  can already use any library through asdf, whether it uses coalton internally
  or not, but it would be nice to depends directly on library in Coalton
  (without relying on CL code).
- I think it's a big deal that any piece of code in Coalton can rely on some
  side-effects, and I guess it will make very hard to do proper library
  dependency management in Coalton for this reason.
- I don't know if I'm being fair, but I think the kind of project that Coalton
  are the most beneficial to are the ones with an already large codebase in CL.
  I don't see the point of starting a new project in Coalton when we already
  have Haskell, Idris, Coq, or even other typed LISP for the task.
